===== Dependencies and Installation =====

Doconce itself is pure Python code hosted at URL:
"http://code.google.com/p/doconce".  Its installation from the
Mercurial (`hg`) source follows the standard procedure:
!bc sys
# Doconce
hg clone https://doconce.googlecode.com/hg/ doconce
cd doconce
sudo python setup.py install
cd ..
!ec

If you make use of the "Preprocess": "http://code.google.com/p/preprocess"
preprocessor, this program must be installed:
!bc sys
svn checkout http://preprocess.googlecode.com/svn/trunk/ preprocess
cd preprocess
cd doconce
sudo python setup.py install
cd ..
!ec
A much more advanced alternative to Preprocess is
"Mako": "http://www.makotemplates.org". Its installation is most
conveniently done by `pip`,
!bc sys
pip install Mako
!ec
This command requires `pip` to be installed. On Debian Linux systems,
such as Ubuntu, the installation is simply done by
!bc sys
sudo apt-get install python-pip
!ec
Alternatively, one can install from the `pip` "source code": "http://pypi.python.org/pypi/pip".

To make LaTeX
documents (without going through the reStructuredText format) you
need "ptex2tex": "http://code.google.com/p/ptex2tex", which is
installed by
!bc sys
svn checkout http://ptex2tex.googlecode.com/svn/trunk/ ptex2tex
cd ptex2tex
sudo python setup.py install
cd latex
sh cp2texmf.sh  # copy stylefiles to ~/texmf directory
cd ../..
!ec
As seen, `cp2texmf.sh` copies some special stylefiles that
that `ptex2tex` potentially makes use of. Some more standard stylefiles
are also needed. These are installed by
!bc sys
sudo apt-get install texlive-latex-extra
!ec
on Debian Linux (including Ubuntu) systems. TeXShop on Mac comes with
the necessary stylefiles (if not, they can be found by googling and installed
manually in the `~/texmf/tex/latex/misc` directory).

The *minted* LaTeX style is offered by `ptex2tex` and popular among
users. This style requires the package "Pygments": "http://pygments.org":
!bc sys
hg clone ssh://hg@bitbucket.org/birkenfeld/pygments-main pygments
cd pygments
sudo python setup.py install
!ec
If you use the minted style, you have to enable it by running
`ptex2tex -DMINTED` and then `latex -shell-escape`, see
the Section ref{doconce2formats}.

For `rst` output and further transformation to LaTeX, HTML, XML,
OpenOffice, and so on, one needs "docutils": "http://docutils.sourceforge.net".
The installation can be done by
!bc sys
svn checkout http://docutils.svn.sourceforge.net/svnroot/docutils/trunk/docutils
cd docutils
sudo python setup.py install
cd ..
!ec
To use the OpenOffice suite you will typically on Debian systems install
!bc sys
sudo apt-get install unovonv libreoffice libreoffice-dmaths
!ec

There is a possibility to create PDF files from reST documents
using ReportLab instead of LaTeX. The enabling software is
"rst2pdf": "http://code.google.com/p/rst2pdf". Either download the tarball
or clone the svn repository, go to the `rst2pdf` directory and
run `sudo python setup.py install`.


Output to `sphinx` requires of course "Sphinx": "http://sphinx.pocoo.org",
installed by
!bc sys
hg clone https://bitbucket.org/birkenfeld/sphinx
cd sphinx
sudo python setup.py install
cd ..
!ec

When the output format is `epydoc` one needs that program too, installed
by
!bc sys
svn co https://epydoc.svn.sourceforge.net/svnroot/epydoc/trunk/epydoc epydoc
cd epydoc
sudo make install
cd ..
!ec

Finally, translation to `pandoc` requires the
"Pandoc": "http://johnmacfarlane.net/pandoc/" program
(written in Haskell) to be installed.
!bc sys
sudo apt-get install pandoc
!ec

__Remark.__ Several of the packages above installed from source code
are also available in Debian-based system through the
`apt-get install` command. However, we recommend installation directly
from the version control system repository as there might be important
updates and bug fixes. For `svn` directories, go to the directory,
run `svn update`, and then `sudo python setup.py install`. For
Mercurial (`hg`) directories, go to the directory, run
`hg pull; hg update`, and then `sudo python setup.py install`.
Doconce itself is frequently updated so these commands should be
run regularly.

